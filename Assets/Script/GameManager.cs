﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public enum GameStatus
{
    menü,
    start,
    finish,
    exit
}

public class GameManager : MonoBehaviour
{
    public UIMaanager uIMaanager;

    public BallController _ballController;


    public GameStatus gameStatus;

    public GameObject FailPanel;
    public GameObject FinishPanel;
    private GameObject _ballParent;
    public GameObject FinishGameobject;
    void Awake()
    {
        _ballParent = GameObject.FindGameObjectWithTag("Parent");
        gameStatus = GameStatus.menü;
    }
    public void Start()
    {

    }

    public void StartGame()
    {
        Invoke("Delay", 1f);
    }
    public void Delay()
    {
        gameStatus = GameStatus.start;

    }
    public void ClickExit()
    {
        gameStatus = GameStatus.exit;
    }
    public void GameFinish()
    {

        gameStatus = GameStatus.finish;

    }
    public void FailRestartGame()
    {
        _ballController.Failpos();

        FailPanel.gameObject.SetActive(false);
        _ballController.fail = false;
        uIMaanager.GameStart();
    }
    public void FinishRestartGame()
    {
        _ballController.FinishPos();
        FinishGameobject.GetComponent<Collider>().enabled = true;
        FinishPanel.gameObject.SetActive(false);
        _ballController.finish = false;
        uIMaanager.GameStart();
    }
    void Update()
    {

    }

}
