﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputManager : MonoBehaviour
{

    public static Action omClickToScreen;

    private float _swipeStartPos;
    private float _swipeEndPos;
    private float _delta;
    private float _swipeDistance;
    private bool _isSwiping = false;

    public GameManager gameManager;
    public GameStatus gameStatus;
    public float SwipeDistance
    {
        get { return _swipeDistance; }
    }
    public float Delta
    {
        get { return _delta; }
    }

    public bool IsSwiping
    {
        get { return _isSwiping; }
    }



    private void Start()
    {
        /*
         * scriptteki methodlarımızın haber almaları gereken aksiynlara ekledim
         */
        DragReport.onBeginDrag += SetStartPos;
        DragReport.onDragging += CalculateSwipeDistance;
        DragReport.onEndDrag += StopSwiping;
    }

    private void Update()
    {
        if (gameManager.gameStatus != GameStatus.start)
        {
            return;
        }
        if (Input.GetMouseButtonDown(0))
        {
            /*mouse tıkladığım anda Onclicktoscreen aksiyonum çalışacak
             * OnClicktoScreen aksiyonu içinde olan methodlar olacaklar sunlardır
             * BallCont içinden; StickTower methodu
             * StickTower methodu içinden; onstick aksiyonu çağırılacak
             * OnStick aksiyonu içinden; SetFalgs methodu ve setPosWhenSticked methodları çalıştırılacak
             */
            omClickToScreen?.Invoke();
        }
    }


    private void SetStartPos(PointerEventData data)
    {
        /*Haber alması gereken aksiyon ekranda ki ilk dokunma anıdır o halde dragreport scirpt içinde onbegindrag aksiyonuna eklenmelidir
         * ve sürükleme başladı durumunu döngüsünü true yapar
         */
        _swipeStartPos = data.position.y;
        _isSwiping = true;
    }

    private void CalculateSwipeDistance(PointerEventData data)
    {
        /*sürükleme mesafesini hesaplayacağına göre sürükleme anından haberdar olmalıdır bu method o halde onDraging aksiyonuna eklerim
         * sürüklemenin lk noktası saklandığı için araddaki mesafe alınnır
         */
        _swipeDistance = (data.position.y - _swipeStartPos);
        if (_swipeDistance > 0)
            _swipeDistance = 0;
        _delta = data.delta.y;
    }

    private void StopSwiping()
    {
        /*
         * sürükleme işlemi bitiirleceğinden ekrandan parmak alındığı anda çalışması gerekir bu nedenle onenddrag  aksiyonuna kaydederim
         */
        _isSwiping = false;
        _delta = 0f;
    }
}

