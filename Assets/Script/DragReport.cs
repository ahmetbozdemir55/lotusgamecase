﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class DragReport : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{

    public static Action<PointerEventData> onBeginDrag;
    public static Action<PointerEventData> onDragging;
    public static Action onEndDrag;


    public void OnBeginDrag(PointerEventData eventData)
    {
        /*Ekrana dokunulduğu anda çağırdığı aksiyon Onbegindrag(sürekleme başladı) aksiyonu ççağırılır
         * Bu Aksiyonun zincirleme nokta bilgilerini haber vereceği method ve aksiyonlar şöyledir
         * InputManger dan ; SetStartPos methodu gelir alınan nokta bilgisini işler
         * Uçuş anındaki bir ilk  dokunmada olabileceğinden SetStartpos dunu destekleyen inputmanager içinde ki update içinde oncilicktoscren aksiyonudur
         */

        onBeginDrag?.Invoke(eventData);
    }

    public void OnDrag(PointerEventData eventData)
    {
        /*
         * sürükleme devam edeerken yalnızca mekanik player konumunu ayarlamalıdır o nedenle onDragging(kaydırma oluyor) aksiyonu çağırılır
         * Bu aksiyon içerisinde yalnızca ınput managerda bu aksiyona eklenen ve bu aksiyondan haber alan CalculateSwipeDistence methodu çaşıltırılır
         */
        onDragging?.Invoke(eventData);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        /*
         * ekrana dokunma işlemi tamamlanıpda player serbest bırakıldıgında olacakları çağırdığımız onendDrag(sürükleme bitti )  aksiyonu çağırılır
         * Bu aksiyon zincirleme ierisinde haber vermesi gereken methoıdlar çalışır bu methodlar şunlardır;
         * BallController içinden ;ApplyJumpForce(serbst kaldığı için fırlatma yapılır)  bu method içerisinde ki onrelase(serbest bırakıldı) aksiyonunu çağırır
         * OnRelase aksiyonun içerisinede  ballcontroller start methodunda eklenen SetJumpFlags methodu çağrılır
         * SetjumpFlags methodu serbert bırakıldıgında ki ya da zıplandığındaki durum döngülerini düzenliyor
         * InputManager içinden; StopSwiming Methodu haber alır ve playerın durum döngüsünü değistirir
         * BallController içinden; ResetParentObject ( Angle blgileri düzeltilir)
         */
        onEndDrag?.Invoke();
    }

}
