﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class UIMaanager : MonoBehaviour
{
    public GameManager gameManager;

    [Header("Panels")]
    public GameObject MenüPanel;
    public GameObject GamePanel;
    public GameObject FinishPanel;
    public GameObject FailPanel;

    public void Awake()
    {

    }
    public void Start()
    {

    }
    public void Update()
    {
        if (gameManager.gameStatus != GameStatus.start)
        {
            return;
        }

    }

    public void GameStart()
    {
        HideAllPanel();
        Invoke("Delaystart",.5f);
    }
    public void Delaystart()
    {
        GamePanel.gameObject.SetActive(true);

        gameManager.StartGame();
    }
    public void GameFinish()
    {
        HideAllPanel();
        FinishPanel.gameObject.SetActive(true);


        gameManager.GameFinish();
    }
    public void GameExit()
    {
        Application.Quit();
    }
    public void GameFail()
    {
        HideAllPanel();
        FailPanel.gameObject.SetActive(true);
    }
    public void FailGameRestart()
    {
        HideAllPanel();

        gameManager.FailRestartGame();
    }
    public void FinishGameRestart()
    {
        HideAllPanel();

        gameManager.FinishRestartGame();
    }
    public void HideAllPanel()
    {
        MenüPanel.gameObject.SetActive(false);
        GamePanel.gameObject.SetActive(false);
        FinishPanel.gameObject.SetActive(false);
    }

}
